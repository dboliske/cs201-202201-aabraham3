package exams.second;
//Akkina Abraham
//4/29/22
//Question 5
import java.util.Scanner;

public class Question5JumpSearch {

	public static int search(Double[] lang, Double value) {
		int step = (int)(Math.sqrt(lang.length)); //=3`
	
		int prev = 0;
		while (lang[Math.min(step, lang.length-1)]<(value)) {
			prev = step;
			step += (int)(Math.sqrt(lang.length));
			if (prev >= lang.length) {
				return -1;
			}}

		while (lang[prev]<(value)){ //acts like a sequential search, this loops through each item  in a section when we know the item  is somewhere within the section
			prev++;
			if (prev == lang.length) {
				System.out.println("min"+Math.min(step, lang.length-1));
				return -1;
			}
		}
		
		if (lang[prev]<=(value)) {
			return prev;
		}
		return -1;
	}
	public static void main(String[] args) {
		Double[] lang = {0.577,1.202,1.282, 1.304, 1.414, 1.618, 1.732, 2.685, 2.718, 3.142};
		Scanner input = new Scanner(System.in);
		System.out.print("Search term: ");
		Double value = Double.parseDouble(input.nextLine());
		
		int index = search(lang, value);
		System.out.println("Postion of value found: "+index); //index=-1 if position not found in array


		input.close();
	}


}
