package exams.second;
//Akkina Abraham
//4/29/22
//array list:user enters value until they enter done, print max and min of array
import java.util.ArrayList;
import java.util.Scanner;

public class Question3ArrayLists {

	public static void main(String[] args) {
		
		ArrayList<Integer> numbers = new ArrayList<>();
		boolean done = false;
		
		do {
			System.out.println("Enter an integer number(enter 'Done' when finished): ");
			Scanner input = new Scanner(System.in);
			String value =(input.nextLine());
				if(!value.equalsIgnoreCase("Done")) {
					numbers.add(Integer.parseInt(value));
				}
				else {
					input.close();
					done=true;
				}
			}while(!done);
		
		int min=numbers.get(0);
		int max=numbers.get(0);
		for(int i=1; i<numbers.size();i++) //loops until the full array is gone through incrementing one by one
		{
			if(min> numbers.get(i)) 
			{ 
				min = numbers.get(i); //min value is replaced by next number that is smaller
			}
		}
		System.out.println("The minimum number in the array is: "+min);
		for(int i=1; i<numbers.size();i++) //loops until the full array is gone through incrementing one by one
		{
			if(max<numbers.get(i)) {
				max = numbers.get(i);
			}
		}
		System.out.println("The maximum number in the array is: "+max);
	}

}
