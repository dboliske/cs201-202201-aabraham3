package exams.second;
//Akkina Abraham
//4/29/22
//question 1.a
//classroom - super class / parent class
public class Classroom {

	protected String building;
	protected String roomNumber;
	private int seats;
	
	public Classroom(){
		building = " ";
		roomNumber=" ";
		seats = 0;
	}
	public Classroom(String building, String number,int seats){
		this.building = " ";
		setBuilding(building);
		this.roomNumber=" ";
		setRoomNumber(number);
		this.seats=0;
		setSeats(seats);
	}
	public void setBuilding(String building) {
		this.building=building;
	}
	public void setRoomNumber(String number) {
		this.roomNumber=number;
	}
	public void setSeats(int seats) {
		if(seats>=0) { //seats value needs to be positive to be set
		this.seats=seats;
		}
	}
	public String getBuilding() {
		return building;
	}
	public String getRoomNumber() {
		return roomNumber;
	}
	public int getSeats() {
		return seats;
	}
	public String toString() {
		return "building: " + building + ",room number: " + roomNumber +",Seats: "+seats; 
	}
	
	

}
