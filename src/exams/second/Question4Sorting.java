package exams.second;
//Akkina Abraham
//4/29/22
//Sort Array of strings given by using selection sort
//{"speaker", "poem", "passenger", "tale", "reflection", "leader", "quality", "percentage", "height", "wealth", "resource", "lake", "importance"}
public class Question4Sorting {

	public static String[] sort(String[] array) { //selection sort
		for(int i=0;i<array.length;i++){
			int min =i;
			for(int j =i+1;j<array.length;j++) {
				if(array[j].compareTo(array[min])<0) { //if element j is less than array minimum, then j is the true minimum and therefore needs to come before 
					min = j;
				}
			}
			if (min != i) { //if min is equal to i, we don't have to do any swapping
				String temp = array[i];
				array[i] = array[min];
				array[min] = temp;
			}
		}
		return array;
		}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
	String[] words = {"speaker", "poem", "passenger", "tale", "reflection", "leader", "quality", "percentage", "height", "wealth", "resource", "lake", "importance"};
		
		words = sort(words);
		
		for (String w  : words){
			System.out.print(w+" ");
		}
		
		}
	

}
