package exams.second;
//Akkina Abraham'
//4/29/22
//Question 2.c --> Circle

public class Circle extends Polygon {

	private double radius;
	
	public Circle() {
		super();
		radius=1;
	}
	public void setRadius(double width) { // double "width" as per UML diagram
		if(width>0) { //radius will only be set if value is positive
		this.radius=width;
		}
	}
	public double getRadius() {
		return radius;
	}
	public String toString() {
		return super.toString()+ radius;
	}
	public double area() {
		return Math.PI*radius*radius;
	}
	public double perimeter() {
		return 2*Math.PI*radius;
	}
}
