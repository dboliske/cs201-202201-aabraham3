package exams.second;
//Akkina Abraham
//4/29/22
//question 1.b
//ComputerLab - child class/subclass
public class ComputerLab extends Classroom {
	
	private boolean computers;
	
	public ComputerLab() {
		super();
		computers = false;
	}
	public ComputerLab(String building,String number,int seats,boolean computers) {
		super(building,number,seats);
		this.computers = false;
		setComputers(computers);
	}
	public void setComputers(boolean computers){
		this.computers=computers;
	}
	public boolean hasComputers() {
		return computers;
	}
	public String toString(){
		return super.toString()+",computers: "+computers;
		
	}

}
