package exams.second;
//Akkina Abraham
//4/29/22
//Question 2.a (Polygon abstract class-super class)
public abstract class Polygon { //abstract class

	protected String name;
	
	public Polygon() {
		name = " ";
	}
	public Polygon(String name) {
		this.name = " ";
		setName(name);
	}
	public void setName(String name) {
		this.name=name;
	}
	public String getName() {
		return name;
	}
	public String toString() {
		return name;
	}
	public abstract double area(); //abstract methods
	public abstract double perimeter(); //abstract methods

}
