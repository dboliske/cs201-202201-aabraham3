package exams.first;
//Question 5
import java.util.Scanner;
public class Pet {
	private String name;
	private int age;
	
	public Pet() {
		name = " ";
		age = 0;
	}
	public Pet(String name, int age) {
		this.name = " ";
		setName(name);
		this.age=0;
		setAge(age);		
	}
	public void setName(String name) {
		this.name = name;
	}
	public void setAge(int age) {
		if(age>0) 
		{
			this.age = age;
		}
	}
	public String getName() {
		return name;
	}
	public int getAge() {
		return age;
	}
	public boolean equals(Object obj) {
		
		if(!(this.name).equals(obj.getName())) {	//forgot how to use object in this case as reference
			
			return false;
		}else if((this.age)!=(obj.getAge())) {
			return false;
		}else return true;
	}
	public String toString() {
		return "Your pet " + name +" is " +age+" years old.";
	}
	
}

//