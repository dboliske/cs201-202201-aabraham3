package exams.first;
import java.util.Scanner;
//Write a program that prompts the user for an integer, add 65 to it, convert the
//result to a character and print that character to the console.
public class Question1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
			System.out.println("Enter any value between 0-25: ");
			Scanner input = new Scanner(System.in);
			int value = Integer.parseInt(input.nextLine());
			input.close();
			int a = value + 65;
			
			System.out.println("The value you entered in character form is: "+((char)a));
					
			
	}

}
