package exams.first;
/*Write a program that prompts the user for an integer. If the integer is divisible by
 *  2 print out "foo", and if the integer is divisible by 3 print out "bar". If the integer 
 *  is divisible by both, your program should print out "foobar" and if the integer is not divisible
 *   by either, then your program should not print out anything.
 */
import java.util.Scanner;
public class Question2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		System.out.println("Enter an integer");
		Scanner input = new Scanner(System.in);
		int a = Integer.parseInt(input.nextLine());
		input.close();
		if(((a%2)==0)&&((a%3)==0)) {
			System.out.println("foobar");
		}
		else if((a%2)==0) {
			System.out.println("foo");
		}
		else if((a%3)==0){
			System.out.println("bar");
		}
		
	}

}
