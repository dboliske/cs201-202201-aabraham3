//Akkina Abraham
//2/13/22
//create the GeoLocation Class using the UML Given
package labs.lab4;

public class GeoLocation {	//Variable/attributes

	private double lat;
	private double lng;
	
	public GeoLocation() { //default constructor
		lat = 0.0;
		lng = 0.0;
	}
	public GeoLocation(double lat, double lng) {	//non-default constructor
		this.lat = lat;
		//setLat(lat);
		this.lng = lat;
		//setLng(lng);
	}
	public double getLat() {	//accessor/getter
		return lat;
	}
	public double getLng() {	//accessor/getter
		return lng;
	}
	public void setLat(double lat) {	//mutator
		//if(lat >= -90 && lat <= 90)
		{
		this.lat = lat;		//setter,sets value for lat
		}
	}
	public void setLng(double lng) {	//mutator
		//if(lng >= -180 && lng<= 180)
		{
		this.lng = lng;		//setter,sets value for lng
		}
	}
	public String toString() {
		return "("+ lat + ","+ lng+ ")"; //<-- prints in (#,#) format as string
	}
	public boolean validLat(double lat) { //checking if latitude inputs are valid 
		return(lat >= -90 && lat <= 90);
//		if(lat>90 ) {	<-- another way to write same thing in line 43
//			return false;
//		}
//		else if(lat< -90) {
//			return false;
//		}return true;
//		
	} 
	public boolean validLng(double lng) {		//checking if longitude inputs are valid 
		return (lng >= -180 && lng<= 180);}

//			if(lng > 180) { <-- another way to write line 53
//				return false;
//			}
//				else if(lng < -180){
//					return false;
//				}return true;
//			
//			}

		
	public boolean equals(GeoLocation g) {	
		if (this.lat != g.getLat()) {	//if latitude measurements are not equal will be false and if true will continue to next if else
			return false;
		}else if(this.lng != g.getLng()) {	//if longitude values are not equal, then false, but if true will now return both lng and lat as true
			return false;
		}return true;
	}
}
	
