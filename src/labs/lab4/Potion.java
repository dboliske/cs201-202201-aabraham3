//Akkina Abraham
//2.17.22
// creates the class Potion based off UML diagram given
package labs.lab4;

public class Potion {
	
	private String name;
	private double strength;
	
	public Potion() {	//default constructor
		name = " ";
		strength = 0.0;
	}
	public Potion(String name, double strength) {	//non-default constructor , one is a string and the other is a double
		this.name = name;
		
		this.strength = 0.0;
		setStrength(strength);
		
	}
	public String getName() 		//getter/accessor for name
	{
		return name;
		
	}
	public double getStrength()		//getter/accessor for strength
	{
		return strength;
	}
	public void setName(String name)		//setter/mutator for name
	{
		this.name = name;
	}
	public void setStrength(double strength)	//setter/mutator for strength
	{ if(strength>0 && strength < 10) 
		{
		
		this.strength = strength;
		}
	}
	
	public String toString()		//returns name and strength as one string 
	{
		return name + strength;
	}
	
	public boolean validStrength(double strength)
	{
		return(strength > 0 && strength <10);
	}	
	
	public boolean equals(Potion p)
	{
		if(!(this.name).equals(p.getName()))		
				{
					return false;
				}else if((this.strength)!= (p.getStrength()))
						{
							return false;
						}return true;
	}

}
