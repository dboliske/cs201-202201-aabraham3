//Akkina Abraham
//2/17/22
//A class based off the Phone Number UML diagram given
package labs.lab4;

public class PhoneNumber {

	private String countryCode;
	private String areaCode;
	private String number;
	
	public PhoneNumber() {		//default constructor set to empty strings
		countryCode = " ";
		areaCode= " ";
		number= " ";
	}
	public PhoneNumber(String cCode,String aCode, String numbers)		//non-default constructor
	{
		countryCode = " ";
		setCountryCode(cCode);
		areaCode = " ";
		setAreaCode(aCode);
		//setAreaCode(aCode);
		number = " ";
		setNumber(numbers);
	}
	public String getCountryCode()		//getter/accessor for country code
	{
		return countryCode;
	}
	public String getAreaCode() 		//getter/accessor for area code
	{
		return areaCode;
	}
	public String getNumber()			//getter/accessor for number
	{
		return number;
	}
	public void setCountryCode(String cCode)	//setter/mutator for Country code
	{
		this.countryCode = cCode;
	}
	public void setAreaCode(String aCode)		//setter/mutator for area code
	{
		if(aCode.length() == 3)
		{
		this.areaCode = aCode;
		}
	}
	public void setNumber(String numbers)		//setter/mutator for numbers
	{	if(numbers.length() == 7)
		{
		this.number = numbers;
		}
	}
	public String toString()		//displays phone number as one string
	{
		return countryCode+ " " + areaCode + " " + number;
	}
	
	
	
	
	public boolean validAreaCode(String aCode)		// validating inputs area code
	{ 
		return aCode.length() == 3; 
	}

	
	public boolean validNumber(String numbers)		// validating inputs numbers
	{
		return numbers.length() == 7;
	}
	
	
	public boolean equals(PhoneNumber pn)		//comparing if phone numbers are equivalent 
	{
		if(!(this.countryCode).equals((pn.getCountryCode())))
		{
			return false;
			
		}else if((this.countryCode).equals(pn.getCountryCode()))
		{if(!(this.areaCode).equals(pn.getAreaCode()))
		{
			return false;
		}else if(!(this.number).equals(pn.getNumber()))
		{
			return false;
		}
			
		}return true;
	}
	
}
