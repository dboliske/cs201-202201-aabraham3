//Akkina Abraham
//2/13/22
//Print the results  (instantiates) of the GeoLocation Class using the UML Given
package labs.lab4;

public class GeoLocationClient {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		
		
		GeoLocation g1 = new GeoLocation();
		g1.setLat(75);
		g1.setLng(100);
		
		GeoLocation g2 = new GeoLocation();
		g2.setLat(65);
		g2.setLng(71);
		
		GeoLocation g3 = new GeoLocation();
		g3.setLat(75);
		g3.setLng(100);

		GeoLocation g4 = new GeoLocation();
		g4.setLat(55);
		g4.setLng(250);
		
		GeoLocation g5 = new GeoLocation();
		g5.setLat(30);
		g5.setLng(250);
		
		System.out.println("g1" + g1);
		System.out.println("g2" + g2);			//checks to see if g2 stored input values
		System.out.println("g3"+g3);
		System.out.println("g4: "+ g4);
		System.out.println("g5" + g5);
		
		System.out.println(g1.equals(g2));	//compares g1 to g2..they are not equal so should print false
		System.out.println(g1.equals(g3)); 	//compares g1 to g3..they are equal so should print true
		System.out.println(g3.validLng(g3.getLng()));	//see if Longitude value are valid for g3 input..they are, so should result true
		System.out.println(g4.validLng(g4.getLng()));	//see if Longitude values are valid for g4 input..they are not so should result false
		System.out.println(g5.validLat(g5.getLat()));	//see if Latitude values are valid(between -90 and 90), they are not, should be true
		System.out.println(g2.validLat(g2.getLat()));	//see if Latitude values are valid(between -90 and 90), they are, should be true
		System.out.println(g5.validLng(g5.getLng()));
		
	}

}
