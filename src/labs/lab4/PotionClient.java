//Akkina Abraham
//2.17.22
//Potion Client instantiated
package labs.lab4;

public class PotionClient {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
			
			Potion p1 = new Potion();		//new potion implemented by pn1
			p1.setName("Forever");
			p1.setStrength(5);
			
			Potion p2 = new Potion();		//new potion implemented by pn2, same contents as pn1
			p2.setName("Forever");
			p2.setStrength(5);
			
			Potion p3 = new Potion();		//new potion implemented by pn3
			p3.setName("Love");
			p3.setStrength(9);
			
			Potion p4 = new Potion();		//new potion implemented by pn4
			p4.setName("Magic");
			p4.setStrength(15);
			
			System.out.println("Valid Strength Check: ");
			System.out.println(p3.validStrength(p3.getStrength()));	//checks validity of strength(between 0-10) of p3
			System.out.println(p4.validStrength(p4.getStrength()));	//checks validity of strength(between 0-10) of p4
			
			System.out.println("Equals Potion Check: ");
			System.out.println(p1.equals(p2));		//checks if p1 is equal (in name and strength) to p2
			System.out.println(p1.equals(p3));		//checks if p1 is equal (in name and strength) to p3
			
			
	}

}
