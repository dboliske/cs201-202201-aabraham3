//Akkina Abraham
//2/13/22
//Print the results(instantiates) of the Phone Number Class using the UML Given
package labs.lab4;

public class ClientPhoneNumber {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		PhoneNumber pn1 = new PhoneNumber();
		
		System.out.println(pn1.toString()); 		//new phone number implemented by pn1
		
		pn1.setCountryCode("1");
		pn1.setAreaCode("847");
		pn1.setNumber("7917832");
		System.out.println(pn1.toString()); 
		
		PhoneNumber pn2 = new PhoneNumber();		//new phone number implemented by pn2
		pn2.setCountryCode("1");
		pn2.setAreaCode("847");
		pn2.setNumber("7917852");
		System.out.println(pn2.toString()); 
		
		PhoneNumber pn3 = new PhoneNumber();		//new phone number implemented by pn3
		pn3.setCountryCode("1");
		pn3.setAreaCode("847");
		pn3.setNumber("7917832");
		System.out.println(pn3.toString()); 
		
		PhoneNumber pn4 = new PhoneNumber();			//new phone number implemented by pn4
		pn4.setCountryCode("1");
		pn4.setAreaCode("8470");
		pn4.setNumber("791782");
		System.out.println(pn3.toString()); 
		
		//boolean validation method execution
		
		System.out.println("Valid Area Code Check:");
		System.out.println(pn1.validAreaCode(pn1.getAreaCode()));		//checks validity of area code in pn1 phone number
		System.out.println(pn1.validAreaCode(pn4.getAreaCode()));		//checks validity of area code in pn4 phone number

		
		System.out.println("Valid Number Check: ");
		System.out.println(pn1.validNumber(pn1.getNumber()));		//checks validity of number in pn1 phone number
		System.out.println(pn1.validAreaCode(pn4.getAreaCode()));	//checks validity of number in pn1 phone number
		
		//boolean equals method execution
		System.out.println("Equal Numbers Check: ");
		System.out.println((pn1.toString()).equals(pn2.toString()));	//checks if phone number pn1 is equal to pn2
		System.out.println((pn1.toString()).equals(pn3.toString()));	//checks if phone number pn1 is equal to pn3
		
	}

}
