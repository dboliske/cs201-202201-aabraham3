//Akkina Abraham
//1.20.22
//perform various arithmetic operations
package labs.lab1;

public class ArithmeticCalculations {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int myAge = 26;
		int myFathersAge = 67;
		int myBirthYear = 1996;
		double CMconversion = 2.54; //the value in centimeter that equals 1 inch
		double myHeightInches = 64.8; //my height in inches

		
		
		
		
		System.out.println("My age subtracted from my father's age is: " + (myFathersAge - myAge)); //my father's age minus my age
		System.out.println("My birth year multipled by two is " + (myBirthYear*2)); //my birth year doubled
		System.out.println("My height in inches is 64.80, but my height in centimeters is:  " + (myHeightInches*CMconversion)); //my height converted to centimeters
		System.out.println("My height in feet and inches is: " + ((int)myHeightInches/12)+ " feet and " + (int)(myHeightInches%12)+ " inches"); // my height in feet and inches
		
		
	}

}
