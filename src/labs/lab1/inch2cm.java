//Akkina Abraham
//1.20.22
//Take user input of inches and convert to centimeters
package labs.lab1;
import java.util.Scanner;
public class inch2cm {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		System.out.println("Enter a number in inches: ");
		Scanner input = new Scanner(System.in);
		double inches = input.nextDouble();
		double CMconversion = 2.54; //the value in centimeter that equals 1 inch
		input.close();
		System.out.println(inches + " inches converted to centimeters is: " + (inches*CMconversion));
		
	}

}

//Test plan:
//input: 655 output received:1663.7, expected output:1663.7
//input: 310 output received:787.4, expected output:787.4
//input: 30  output received:76.2, expected output:76.2
