//Akkina Abraham
//1.20.22
//Converting user input from Fahrenheit to Celsius and then vice versa
package labs.lab1;
import java.util.Scanner;

public class TempConversion {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println("Enter a degree in Fahrenheit: ");
		Scanner input = new Scanner(System.in);
		float Fahrenheit = input.nextFloat(); //read in fahrenheit value entered by user
		
		System.out.println("The degrees in fahrenheit entered ("
		+ Fahrenheit+ ") converted to degrees in Celsius is: "+((Fahrenheit-32)*5/9)); //outputs conversion as well as calculates conversion here
		
		//Test plan
		//input:10 output received:-12.222, EXPECTED outpuT:-12.222
		//input:50 output received:10, EXPECTED output:10
		//input:150 output received:65.55556, EXPECTED output:65.55556
		
		
		
		System.out.println("Enter a degree in Celsius"); //User input for celsius value
		Scanner in = new Scanner(System.in);
		float Celcius = in.nextFloat(); 
		System.out.println("The degree in Celcius is ("+ Celcius +") converted to degrees in Fahrenheit is: "+((Celcius*9/5)+32)); //output celsius conversion value and calculates here 
		
		input.close(); 
		in.close();
		
		//Test plan
		//input:3 output received:37.4, EXPECTED output:37.4
		//input:50 output received:122., EXPECTED output:122.
		//input:150 output received:302, EXPECTED output:302
	}

}
