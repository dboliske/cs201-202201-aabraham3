//Akkina Abraham
//1.20.21
//use user input for width,depth, and height of a box and then estimate cost of wood for box
package labs.lab1;
import java.util.Scanner;
public class Box {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println("Enter a width,length, and depth for your box in inches: ");
		Scanner input = new Scanner(System.in);
		float width = input.nextFloat();
		float length = input.nextFloat();
		float depth = input.nextFloat();
		
		System.out.println(width);
		System.out.println(length);
		System.out.println(depth);
		
		System.out.println("Two (length*width) panels of wood "+(length*width*2)+" sq in + two (depth*width) panels of wood "+ 
		(depth*width*2)+"\nsq in + two (length*depth) panels of wood "+ (length*depth*2)+" sq in = " + 
		(((width*length*2)+(depth*width*2)+(length*depth*2))/144)+ " sq ft of wood");
		
		//Input(inches): 10 L,10 W, 10 D Output:(in sq ft):4.1666665, EXPECTED Output:(in sq ft):4.1666665
		//Input(inches): 10 L, 5 W, 5 D  Output:1.7361111, EXPECTED Output:(sq ft):1.7361112
		//Input(inches): 15 L, 4 W, 3 D Output:1.625, EXPECTED Output:(sq ft):1.625 
		
		
		
		
		
		
		
	}

}
