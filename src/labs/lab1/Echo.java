//Akkina Abraham
//1.20.21
// Have the user input a name and the console echo the name when

package labs.lab1;

import java.util.Scanner;
public class Echo {

public static void main(String[] args) {
		
		System.out.println("Enter your name: ");  //user enters in name here
		Scanner input = new Scanner(System.in);
		String name = input.nextLine();
		 
		input.close();								//close scanner
		 
		 System.out.println("Your name is "+ name); //console prints name given
		 
		
		
		
	
	
	
	}
}