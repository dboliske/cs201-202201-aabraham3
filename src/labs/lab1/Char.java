//Akkina Abraham
//1.20.22
// Display the first letter of the name entered by user
package labs.lab1;
import java.util.Scanner;


public class Char {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println("Enter your name: ");  //asks user for name
		Scanner input = new Scanner(System.in);
		String name = input.nextLine();
		
		input.close();							//closes scanner
		
		System.out.println(name.charAt(0));		//displays first character of name user inputed 
	}

}
