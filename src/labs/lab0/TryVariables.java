//Akkina Abraham
//1.13.22
//Debug program TryVariables 
package labs.lab0;

public class TryVariables {

	public static void main(String[] args) {
		// This example shows how to declare and initialize variables

		int integer = 100;
		long large = 42561230L;
		int small = 25;	//variable not properly declared 
		byte tiny = 19;

		float f = .0925F; //forgot the semicolon at end of the statement
	    double decimal = 0.725;
		double largeDouble = +6.022E23; // This is in scientific notation

		char character = 'A';
		boolean t = true;

		System.out.println("integer is " + integer);
		System.out.println("large is " + large);
		System.out.println("small is " + small); //system was not capitalized
		System.out.println("tiny is " + tiny);	//variable tiny was spelled wrong as "tine"
		System.out.println("f is " + f);
		System.out.println("decimal is " + decimal); //used a comma instead of a period
		System.out.println("largeDouble is " + largeDouble);
		System.out.println("character is " + character); //forgot the "+" symbol before character
		System.out.println("t is " + t);

	}

}