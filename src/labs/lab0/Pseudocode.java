//Akkina Abraham
//1.13.22
// Create a pseudocode that outputs a square filled or hollow
package labs.lab0;

public class Pseudocode {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		/* PSEUDOCODE:
		print(****)		//first row
		print(\n)		//will cause the next statement to print on the next line below
		print(****)		//second row
		print(\n)
		print(****)		//third row
		print(\n)
		print(****)		//fourth row .....prints a 4x4 square
		*/
		
		System.out.println("****" + "\n" + "****"+"\n"+"****" + "\n" + "****"); //it did output a 4x4 square
	}

}

