package labs.lab6;
//Akkina Abraham
//3.25.22 (Extension due to accomodations)
//The purpose of this class was to create a repeating menu that would be executed so that customers at a deli could be added to and then removed from an array list keeping track of their place in line
import java.util.ArrayList;
import java.util.Scanner;

public class DeliClient {


	public static ArrayList<String> menu(Scanner input, ArrayList<String> data){ //passing in scanner and data arrayList from main 
	
	boolean done = false;
	String[] options = {"Add Customer to Queue","Help Customer","Exit"};
	do {
		
		for(int i=0; i<options.length;i++) {
			System.out.println((i+1)+". "+options[i]);	//prints out menu options that were stored in array "options" above
		}
		System.out.println("Choice: ");
		String choice = input.nextLine();
		
		switch(choice) {
		case "1":
			/*This menu option should prompt the user for a name (String) to be added to
			 *  the end of an ArrayList of customers and returns the customers position in the queue.*/
			data=addCustomer(input,data);
			break;
		case "2":
			/*This should remove the customer from the
			 *  front of the queue (position 0) and prints out that customers name.
			 */
			System.out.println("You've selected to 'Help customer in queue'...");
			data = removeCustomer(input,data);
			break;
		case "3":
			done= true;
			break;
		default:
			System.out.println("That was not an option...");
		}
	}while(!done);
	System.out.print("Goodbye!"); //when exited from menu goodbye will print to let user know menu was exited successfully
	return data;
	}
	public static ArrayList<String> addCustomer(Scanner input, ArrayList<String> data){
		System.out.println("Enter the name of the newest customer in line: ");
		String newName = input.nextLine();
		data.add(newName);

		System.out.println("Position in line: "+data.indexOf(newName));
		return data;
	}
	public static ArrayList<String> removeCustomer(Scanner input, ArrayList<String> data){
		int count = 0;
		
		if (count==data.size()) {	
			/* if they amount of customers equal the size of the array, no more customers
			 * are left in the array. we note and return to the main menu here to avoid error*/
			System.out.println("There are no more customers to be helped!");	
			return data;
		}
		System.out.println("Customer "+data.remove(0)+" has been helped.Thank you!");
		count++;
		
		return data;
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);
		ArrayList<String> data = new ArrayList<String>(); //automatically sets to array capacity of 10
		data = menu(input,data);	//calls menu method and executes it.
		input.close();
	}
}



