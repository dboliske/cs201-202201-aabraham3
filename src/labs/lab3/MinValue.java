//Akkina Abraham
//2.2.22
//Write a program that chooses the minimum value of an array that was given
package labs.lab3;
public class MinValue 
{

	public static void main(String[] args)
	{
		// TODO Auto-generated method stub
		int[] array =new int[]{72, 101, 108, 108, 111, 32, 101, 118, 101, 114, 121, 111, 
				110, 101, 33, 32, 76, 111, 111, 107, 32, 97,116, 32, 116, 104, 101, 115, 
				101, 32, 99, 111, 111, 108, 32, 115, 121, 109,98, 111, 108,115, 58, 32, 
				63264, 32, 945, 32, 8747, 32, 8899, 32, 62421}; 
		//save the array we were given
		
		int min=array[0];										
		//set the initial minimum of the array to the first value so that it is compared to every other value
		
			for(int i=1; i<array.length;i++) //loops until the full array is gone through incrementing one by one
				{
					if(min> array[i]) 
					{ 
						min = array[i]; //min value is replaced by next number that is smaller
					}
				}
		System.out.print(min);
	}

}
