//Akkina Abraham
//02.02.22
//"src/labs/lab3/grades.csv". 
//Write a program that reads in the file and computes the average grade for the class and then prints it to the console.
package labs.lab3;
import java.io.IOException;
import java.util.Scanner;
import java.io.File;

public class AverageGrade {

	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
		File f = new File("src/labs/lab3/grades.csv"); //locates file
		Scanner input = new Scanner(f);					// reads in file
		
		float total=0;									//declare total variable
		int[] grades = new int[14];						//declare array
		int count=0;									//declare counter
		
		while(input.hasNextLine()) {					//reads in file while it still has another line to read, then stops
			String line = input.nextLine();				//stores the line in string variable
			String[] values = line.split(",");			//splits the line string that was read into a string array itself, values being split by commas
			grades[count] = Integer.parseInt(values[1]);// stores split value into an int array as an int, counting each input
			count ++;
		
			
			
		}
		input.close();								//closes scanner
		for(int i=0;i<grades.length;i++) {
			total = total+ grades[i];				//adds grades collected into one float variable, one by one
			
			
		}System.out.println("The average grade is: "+(total/count));  //prints average
		
		
	}

}
