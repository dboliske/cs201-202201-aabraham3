package labs.lab5;
//all this happens in memeory, saving from file to the memory and then we  can use the methods here (set) to overwrite info saved in memory 
public class CTAStation extends GeoLocation {
	private String name;
	private String location;	
	private boolean wheelchair;
	private boolean open;
	
	public CTAStation() {
		super();
		name = "";
		location = "";
		wheelchair = false;
		open = false;
		
	}public CTAStation(String name, double lat, double lng, String location, boolean wheelchair, boolean open){
		super(lat,lng);
		this.name = "";
		setName(name);
		this.location = "";
		setLocation(location);
		this.wheelchair = false;
		setWheelchair(wheelchair);
		this.open = false;
		setOpen(open);
	}
	public String getName() {
		return name;
	}
	public String getLocation() {
		return location;
	}public boolean hasWheelchair() {
		return wheelchair;
	}public boolean isOpen(){
		return open;
	}public void setName(String name) {
		this.name = name;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public void setWheelchair(boolean wheelchair) {
		this.wheelchair =wheelchair;
	}
	public void setOpen(boolean open) {
		this.open=open;
	}
	public String toString() { 
		
		return  "Lat,Lng: "+super.toString() + "name: "+ name +"wheelchair: " + wheelchair + "location: "+ location +"open: "+ open; 		
		}
public boolean equals(Object obj) {
	if (!super.equals(obj)) {
		return false;
	} else if (!(obj instanceof CTAStation)) {
		return false;
	}
	
	
	return true;
}
}
//