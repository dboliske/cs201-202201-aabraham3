//Akkina Abraham
//3.8.22
//i created a geolocation class that had the updated methods of calcDistance(2 versions)
package labs.lab5;
public class GeoLocation {	

	private double lat;
	private double lng;
	
	public GeoLocation() { //default constructor
		lat = 0.0;
		lng = 0.0;
	}
	public GeoLocation(double lat, double lng) {
		this.lat = lat;
		setLat(lat);
		this.lng = lng;
		setLng(lng);
	}
	public double getLat() {
		return lat;
	}
	public double getLng() {
		return lng;
	}
	public void setLat(double lat) {
		if(lat >= -90 && lat <= 90)
				{
				this.lat = lat;		//setter,sets value for lat
				}
			}
	public void setLng(double lng) {
		if(lng >= -180 && lng<= 180) {
			this.lng = lng;
		}
	}public String toString() {
		
		return "("+ lat + ","+ lng+ ")";
	}public boolean validLat(double lat) {
		return(lat >= -90 && lat <= 90);
	}public boolean validLng(double lng) {
		return(lng >= -180 && lng<= 180);
	}
	public boolean equals(GeoLocation g) {	
		if (this.lat != g.getLat()) {	//if latitude measurements are not equal will be false and if true will continue to next if else
			return false;
		}else if(this.lng != g.getLng()) {	//if longitude values are not equal, then false, but if true will now return both lng and lat as true
			return false;
		}return true;
	}

	/*
	 * A method called calcDistance that takes another GeoLocation and returns a
	 * double. Note: This should use the formula Math.sqrt(Math.pow(lat1 - lat2, 2)
	 * + Math.pow(lng1 - lng2, 2)). A method called calcDistance that takes a lng
	 * and lat and returns a double. Note: See above formula.//
	 */
	public double calcDistance(GeoLocation g) {	//both of these methods are the same, just written differently.
		//Math.sqrt(Math.pow(lat1 - lat2, 2)+ Math.pow(lng1 - lng2, 2))
	
		
		return Math.sqrt(Math.pow(this.lat-g.getLat(),2)+Math.pow(this.lng - (g.getLng()), 2));
	}
	public double calcDistance(double lat, double lng) {
		//System.out.println("This.lat:  "+this.lat); // is the value we pass in through console
		//System.out.println("lat:  "+lat); //is the lat from csv
		return Math.sqrt(Math.pow(this.lat -lat ,2)+Math.pow(this.lng - lng, 2));
	}
}
