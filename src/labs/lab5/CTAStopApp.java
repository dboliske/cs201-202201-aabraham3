package labs.lab5;
//Akkina Abraham///////
//3.8.22
/*A CTA stop application the utilizes the super class geolocation 
and sub class CTAStation. I created a menu that allows the user 
to get information about CTAStations*/
import java.util.Scanner;
import java.io.File;
import java.io.FileNotFoundException;



public class CTAStopApp {

	public static CTAStation[] readFile(String filename) {
		CTAStation[]  station = new CTAStation[10]; 	//station is name of array now
		int count = 0;
		
		try {
			File f = new File(filename);
			Scanner input = new Scanner(f);
			
				input.nextLine();
			
			try {
				
				while(input.hasNextLine()) {
		
				String line = input.nextLine();
				String[] values = line.split(",");
				
					if(station.length==count) 
					{
						station = resize(station, station.length*2);	//resizes array to be larger(more than needed) to fit all data being read in
					}
					
				station[count]= new CTAStation(values[0],Double.parseDouble(values[1])	//0=name,lat,lng,location,wheelchair,open
				,Double.parseDouble(values[2]),values[3],Boolean.parseBoolean(values[4]),Boolean.parseBoolean(values[5]));	
				
				count++;

				}
				
				
			
			}
			catch(Exception e)
				{
					e.getMessage();
					System.out.println(e);
				
				}
			input.close();
			}
		catch(FileNotFoundException fnf)
			{
			System.out.println( "file not found");
			}
		station = resize(station,count);	//shrinks array to exact size of data received
		return station;
		}
		
		
	public static CTAStation[] resize(CTAStation[] station, int size) 
		{
		
		CTAStation[] temp = new CTAStation[size];
		int limit = station.length>size? size : station.length;//if no keep size, yes data.length adjusts

			for (int i=0;i<limit;i++){
				temp[i]=station[i];	//copies exact number of values with info into temp array to create array of exact size needed
				}
	
		return temp;
		
		}

	public static CTAStation[] menu(Scanner input, CTAStation[] station) {	//menu for user to choose selection of info needed
		boolean done = false;
	
		while(!done) {
			try {
			System.out.println("\n1.Display Station Names");
			System.out.println("2.Display Station with/without wheelchair access");
			System.out.println("3.Display Nearest Station");
			System.out.println("4.Exit");
			System.out.println("choice: ");
			
			String choice = input.nextLine();
			
			switch(choice) {
			case "1":
				//display station names
				
				 displayStationNames(station);
				break;
			case"2":
				//display stations w or w/o wheel chair access
				
				displayByWheelchair(station,input);
				break;
			case "3":
				//display  nearest station to user input location (in coordinates)
				displayNearest(station,input);
				break;
			case"4":
				done = true;
				break;
			default:
					System.out.println("I am sorry, I did not understand that input");
			}
			}catch(Exception a) {
				System.out.println("Something went wrong calling display method");
				a.getMessage();
				System.out.println(a);
			}
			
		} 
		return station;
		
	}
	
	public static CTAStation[] displayStationNames(CTAStation[] station) {
		
		for(int i=0; i<(station.length);i++) {
			System.out.println(station[i].getName());
		}
		
		return station;	
	}
	public static CTAStation[] displayByWheelchair(CTAStation[] station, Scanner input) {
		boolean done = false;
		
		do	{
				boolean found = false;
				System.out.println("Do you need wheelchair access?(Enter y or n): "); //in do loop so 
				String choice = input.nextLine();
					if(choice.equalsIgnoreCase("y")) {
						for(int i =0;i<station.length;i++) {
						if((station[i].hasWheelchair())==true) {
							found=true;
						System.out.println("Station Name: "+station[i].getName()+"\nWheelchair Access:Yes ");}
						}	
						if(found==false) {
							System.out.println("No stations found");
						}
						
						done=true;
					}
					else if(choice.equalsIgnoreCase("n")) {
						for(int i =0;i<station.length;i++) {
							
							if((station[i].hasWheelchair())==false) {
								found=true;
							System.out.println("Station Name: "+station[i].getName()+"\nWheelchair Access:No ");
							}
							
						}
						if(found==false) {
							System.out.println("No stations found");
						}
						
						done= true;
					}
			}while(!done);
		
		return station;
		
	}
	public static CTAStation[] displayNearest(CTAStation[] station, Scanner input) {
		System.out.println("Enter a latitude coordinate: ");
		Double latitude= Double.parseDouble(input.nextLine());
		System.out.println("Enter a longitude coordinate: ");
		Double longitude= Double.parseDouble(input.nextLine());
		String ClosestStation = " ";
		double minimumDistance=0;
		for(int i=0;i<station.length;i++)
		{
		
			
			double ctaLat= station[i].getLat();
			double ctaLng = station[i].getLng();
					
			GeoLocation userlocation = new GeoLocation(latitude,longitude);	//for calcDistance(lat,lng)
			double distance =userlocation.calcDistance(ctaLat,ctaLng);
			
			//GeoLocation g = new GeoLocation(ctaLat,ctaLng);			//for calcDistance(geolocation g)...verified with test numbers to make sure it works.
			//double distance2 =station[i].calcDistance(n); used to verify values
			//System.out.println("dISTANCE:"+distance2); used to verify values
			
			if(i==0) {											//changing initial minimum to distance of user and first location given in csv file
				minimumDistance=station[i].calcDistance(userlocation);
				ClosestStation = station[i].getName(); //change initial closest station name to first station in csv file to be able to be compared with the rest later
			}
			if(minimumDistance>(distance)) {	//compares first distance with user vs all other distances, if any distance is shorter/nearer, that becomes the new min distance feom user.
				minimumDistance= distance;
				ClosestStation = station[i].getName(); //in the for loop, the value that is most near(the latest minimum set) , is the location that is closest in proximity to user
			}
			
		}
		System.out.println("Closest station: " + ClosestStation+ " Distance: "+minimumDistance);
		return station;
		}
public static void main(String[] args) //main method should be as short as possible
		{
	
		Scanner input = new Scanner(System.in);	
		CTAStation[] station = readFile("src/labs/lab5/CTAStops.csv"); //reads file in to the object, station.
		menu(input,station);	//allows user to gather information on CTAStation using menu options. Station object is that was created using the read in data is passed in to the menu for data to continue to be used.
		input.close();
		}
}
