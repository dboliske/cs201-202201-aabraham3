//Akkina Abraham
//1.26.22
//Create a menu that is repeatedly displayed and the user can pick an option, each option carrying out its specified task and printed to the console
package labs.lab2;
import java.util.Scanner;
public class Menu {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in); //scans in menu selection
		Scanner now = new Scanner(System.in); //without second scanner, the 2nd and third option repeats menu twice because of original scanner being used...
											 //scans in values entered in subsections
		boolean done = false;
		String select = "";					//creates/declares string variable that can be used throughout entire program
		while(!done) {						//while loop
			
			System.out.println("1.Say Hello");			//menu options
			System.out.println("2.Addition");
			System.out.println("3.Multiplication");
			System.out.println("4.Exit"+"\n");
			System.out.println("Select: ");
			select = input.nextLine();					//reads in user's menu selection
			
	
			switch(select) {							//uses the user's input to pick which case needs to be executed
			case "1":
				System.out.println("Hello"+"\n");		//prints hello to console
				break;
			case "2":
				System.out.println("Enter value 1: ");	
				int a = now.nextInt();					//enters in value 1 for addition
				System.out.println("Enter value 2: ");	
				int b = now.nextInt();					//enters in value 2 for addition
				System.out.println("The sum is: " + (a+b) + "\n"); //prints and executes addition of two values
				
				break;
			case "3":
				System.out.println("Enter value 1: ");	//enters in value 1 for multiplication
				a = now.nextInt();
				System.out.println("Enter value 2: ");	//enters in value 2 for multiplication
				b = now.nextInt();
				System.out.println("The product is: "+(a*b)+ "\n");	//prints and executes multiplication of two values
				break;
			case "4":
				done = true;							
				break;
			} 
		} System.out.println("Exited Succesfully");

	}

}
