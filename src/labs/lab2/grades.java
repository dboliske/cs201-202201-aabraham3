//Akkina Abraham
//1.25.22
//User enters in grades and then the average of entered grades are computed and shown in console
package labs.lab2;
import java.util.Scanner;
public class grades {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
			Scanner input = new Scanner(System.in);
			float grade=0;
			float totalgrade = 0;
			int count = 0;
			Boolean done=false;

			
			while(!done){
				System.out.print("Enter a grade value (Enter -1 when finished entering grades): ");
				grade = Float.parseFloat(input.nextLine());
				if(grade==(-1)) {								//breaks the input cycle (while loop) when user is done entering grades
					done = true;
					System.out.println("The grade average is: " +(totalgrade/count));}
				totalgrade += grade ;	//adds grades inputed
				count ++; 				//keeps track of # of grades entered
				
			
				
			}input.close();
			
			//Test plan
			//input:30,40,50,60 output received:45.0, EXPECTED output:45.0
			//input:55,25,91,62,53 output received:57.2, EXPECTED output:57.2
			//input:21.2,95.3,45.4 output received:53.966663, EXPECTED output:53.966666666667 --some rounding error here
			
			
		
		
		
		
	}
	
}
