//Akkina Abraham
//1.25.22
// create a square of dimension entered in by user
package labs.lab2;

import java.util.Scanner;
public class square {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.print("Enter a number: ");
		Scanner input = new Scanner (System.in);
		int number = Integer.parseInt(input.nextLine()); //user inputs desired amount or row/columns to create square.
														//also integer.parseInt() reads in string input as number
		
		for(int i = 0;i<number;i++) { 				//creates rows of square
			for(int y = 0; y<number;y++){			//creates columns of square
				System.out.print("*");
				}System.out.println("");			//Separates rows
			}
		 
		
		input.close();								//closes scanner

	}
}
