package labs.lab7;
//Akkina Abraham
//4/4/22
//Use insertion sort to sort array of strings given
public class InsertionSort {

	public static String[] sort(String[] things) {
		for (int j=1;j<things.length;j++){ //j keeps track of where it starts from
			//i is keeping track of where its being moved down to 
			int i =j;
			while(i>0 && things[i].compareTo(things[i-1])<0) { //while its not in the very beginning of the array and stil not in the correct spot, we want to keep swapping it down
				String temp = things[i];	//swapcode
				things[i] = things[i-1];
				things[i-1] = temp;
				i--; //because we moved our new inserted vale down by one position
			}
		}
		return things;
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String[] things = {"cat", "fat", "dog", "apple", "bat", "egg"}; //created array of things to be sorted
		
		things = sort(things); //updates array with sorted list 
		
		for(String l: things) {
			System.out.print(l+ " ");
	}
	
}
}

