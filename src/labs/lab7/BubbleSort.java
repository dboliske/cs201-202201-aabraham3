package labs.lab7;
//Akkina Abraham
//4/4/22
//To use bubble sort algorithm to sort array of ints given
/*
 * Write a Java program that will implement the Bubble Sort algorithm as a method and sort the following array of integers:
{10, 4, 7, 3, 8, 6, 1, 2, 5, 9}
 */
public class BubbleSort {
	
	public static int[] sort(int[] numbers) {
		boolean swapped = false;
		do {
			swapped=true;
		for(int i =0; i<numbers.length-1;i++) {
			if(numbers[i+1]<(numbers[i])) {
				int temp = numbers[i+1];
				numbers[i+1] = numbers[i];
				numbers[i]=temp;
				swapped=false;
			}
		}
		}while(!swapped);
		
		
		
		return numbers;
		
	}

	public static void main(String[] args) {	//main body
		// TODO Auto-generated method stub
		int [] numbers = {10,4,7,3,8,6,1,2,5,9}; //creating integer array
		
		numbers = sort(numbers); //type casting
		
		for (int l : numbers) {
			System.out.print(l + " ");
		}
	}

}
