package labs.lab7;

import java.util.Scanner;

//Akkina Abraham
//4/8/22
//Write a Java program that will implement the Binary Search algorithm as a recursive method and be able to search the
//following array of Strings for a specific value, input by the user:{"c", "html", "java", "python", "ruby", "scala"}
public class BinarySearchRecursion {

	// Recursive implementation of the binary search algorithm to return
    // the position of value
    public static int BinarySearch(String[] l, String value)
    {
    	int left = 0;
        int right = l.length - 1;
        System.out.println(l.length);
    	int position =-1;
        // Base condition (search space is exhausted)
        if (left>(right)) {
            return position;
        }
 
        // fin;d the mid-value in the search space and
        // compares it with the target
 
        int mid = (left + right) / 2;
        position=mid;
        
 
        // Base condition (value is found)
        if (value == l[mid]) {
            return mid;
        }
 
        // discard all elements in the right search space,
        // including the middle element
        else if (value.compareTo(String.valueOf(mid))<0) {
            return BinarySearch(l, left, mid - 1, value);
            
        }
 
        // discard all elements in the left search space,
        // including the middle element
        else {
            return BinarySearch(l, mid + 1, right, value);
        }
    }
 
    public static void main(String[] args)
    {
    	String[] l = {"c", "html", "java", "python", "ruby", "scala"};
    	
    	Scanner input = new Scanner(System.in);
		System.out.print("Search term: ");
		String value = input.nextLine();
 
        int left = 0;
        int right = l.length - 1;
        System.out.println(l.length);
      
       int index = BinarySearch(l, value);
 
        if (index!=-1) {
            System.out.println("Element found at index " + index);
        }
        else {
            System.out.println("Element not found in the array");
        }
    }
}


