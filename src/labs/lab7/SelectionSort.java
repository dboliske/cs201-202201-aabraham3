package labs.lab7;
//Akkina Abraham
//4.8.22
//Use selection sort to sort array of doubles given {3.142, 2.718, 1.414, 1.732, 1.202, 1.618, 0.577, 1.304, 2.685, 1.282}
public class SelectionSort {

	public static Double[] sort(Double[] decimals) {
		for(int i=0;i<decimals.length;i++){
			int min =i;
			for(int j =i+1;j<decimals.length;j++) {
				if(decimals[j].compareTo(decimals[min])<0) { //if element j is less than array minimum, then j is the true minimum and therefore needs to come before 
					min = j;
				}
			}
			if (min != i) { //if min is equal to i, we don't have to do any swapping
				Double temp = decimals[i];
				decimals[i] = decimals[min];
				decimals[min] = temp;
			}
		}
		return decimals;
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Double[] decimals = {3.142, 2.718, 1.414, 1.732, 1.202, 1.618, 0.577, 1.304, 2.685, 1.282};
		
		decimals = sort(decimals);
		
		for (Double l : decimals){
			System.out.print(l+" ");
		}
		}

}
