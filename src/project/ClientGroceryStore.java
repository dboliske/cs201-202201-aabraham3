package project;
//Akkina Abraham
//4/28/22
//THE CLIENT APP for the grocery store where the client can access and modify the grocery store inventory. We read, and write to to the csv file here. Client can add, remove, search for, and/or modify inventory here.
import java.io.File;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Pattern;
import project.GroceryItem;
import project.PerishableItem;
import project.AgeRestrictedItem;
import project.ModifyItems;


public class ClientGroceryStore {
	private static ArrayList<GroceryItem> readfile(String filename) {
		ArrayList<GroceryItem> items = new ArrayList<GroceryItem>(); //create an array list initially at original 10 capacity
		GroceryItem b=null;
		try {
			File f = new File(filename);
			Scanner input = new Scanner(f);
			
		while(input.hasNextLine()) { //while loop will continue as long as their is another line to read from the csv file
			String line = (input.nextLine()); 
			String[] values = line.split(","); //splits the values of the line by a comma so that we get the values for name price and expiration/age as their own variables instead of all being read in as a string
			String datePattern = "([1-9]|[0][1-9]|[1][0-2])(-|/)"
					+ "([1-9]|[0][1-9]|[12][0-9]|[3][01])(-|/)"
					+ "([0-9]+)";
			
			try{
				if(values.length<3){ //if it only has name and price its saved as a grocery item (nonperishable item)
					b = new GroceryItem(values[0],Double.parseDouble(values[1]));
					
				}
				else if(Pattern.matches(datePattern,values[2])) { //if it has 3 variables, we need to check if third variable is a date, it if is save the object as a perishable item
					b = new PerishableItem(values[0],Double.parseDouble(values[1]),values[2]);	
					//System.out.println(b);
				}
				
				else if(values[2]!= null) { //if its not a perishbale item or nonperishable item then its a age restricted item
					b = new AgeRestrictedItem(values[0],Double.parseDouble(values[1]),Integer.parseInt(values[2]));
					//System.out.println(b);
				}
				else{ //otherwise something that is not the definition of a grocery item was inserted
				System.out.println("Not a valid grocery item");
				}
			}catch(Exception e) { //error handling for storing objects in proper classification
				System.out.println("Something went wrong storing the objects...");
			}
			items.add(b); //add each object back into the arraylist
			                            
			}
		   }
		catch(Exception e){ //error handling for reading in the file
			System.out.println("Something went wrong reading the file"); //if exception occurs when reading in the file, this will prevent the code from crashing and alerting user where an error occured
		}

		return items;
	}
	public static ArrayList<GroceryItem> menu(Scanner input, ArrayList<GroceryItem> data) {
		//this is the main menu to be able to add, remove, search for, or modify the inventory of the grocery store
		boolean done =false;
		//array of menu options
		System.out.println("\nMain menu!");
		String [] options = {"Add a New Item to the Store Inventory", "Remove an Item from the Store Inventory", "Search for an Item in the Store Inventory", "Modify an Item in the Store Inventory", "Exit"};
		
		do{
			for(int i=0;i<options.length;i++) {	//prints numbered menu in do loop until the boolean is true(exit is selected)
				System.out.println((i+1)+". "+options[i]);
			}
			System.out.println("Enter # of Choice: ");
			String choice = input.nextLine();
			switch(choice) {
			case "1": //adding an item to the store inventory
				System.out.println("You've chosen to add a new item in the store inventory...");	
				data = addItem(data,input);
				break;
			case "2": //removing an item from the store inventory
				System.out.println("You've chosen to remove/purchase an item from the store inventory...");
				data = removeItem(data,input);
				break;
			case "3": //search store inventory for a specific item
				System.out.println("You've chosen to search for an item in the store inventory...");
				searchItem(data,input);
				break;
			case "4": //modifying all instances of an item in the store inventory
				System.out.println("You've chosen to modify an item in the store inventory...");
				ModifyItems.modifyMenu(data,input);

				break;
			case "5": //exit menu
				System.out.println("You've chosen to exit the program...");
				done=true;
				break;
			default: //an invalid option was entered
				System.out.println("That was not a valid choice, try again!");
				break;
			}
		}while(!done);
		System.out.println("Exiting menu!");
		return data;
	}
	public static ArrayList<GroceryItem> addItem(ArrayList<GroceryItem> data, Scanner input){
		//this method will add a new item to the store inventory by first asking the user to input the name and informing us if what type of grocery item is being added(perishable,nonperishable,age-restricted)
		GroceryItem a;
		System.out.print("New item name: ");
		String name = input.nextLine(); //all types of product need a name
		System.out.print("Item price: ");
		double price = 1.0; //initialize price
		try {
			price = Double.parseDouble(input.nextLine()); //all types of product need a price
		} catch (Exception e) {
			System.out.println("Not a valid price. Lets return to menu.");
			return data;
		}
		
		System.out.print("Type of Grocery Item (Perishable, AgeRestricted, or Nonperishable): ");
		String type = input.nextLine();
		switch (type.toLowerCase()) { //need a switch case because not all products have same variables, for example expiration vs age
			case "nonperishable":
				a = new GroceryItem(name,price);
				data.add(a);
			return data;
			case"perishable":
				System.out.print("When does the item expire(mm/dd/yy): ");
				String expiration =input.nextLine();
				a = new PerishableItem(name,price,expiration); //creates new perishable item
				data.add(a); //adds it to the data 
				return data;
			case "agerestricted":
				System.out.print("When is the age required to buy this item: ");
				int age=Integer.parseInt(input.nextLine());
				a = new AgeRestrictedItem(name,price,age); //creates new age restricted object
				data.add(a); //adds it to the data
				return data; //returns data and goes back to menu
			default:
				System.out.println("That is not a valid Grocery Item. Lets return to menu.");
				return data;
		}
	}
	
	public static ArrayList<GroceryItem> removeItem(ArrayList<GroceryItem> data, Scanner input){
		//This method removes any item from the grocery store inventory by have the user input the item they are looking for, searching for that item to validate that it is in the inventory
		//then needing to add the item once found into a cart, and when customer is done adding items to the cart, they can "checkout" by purchasing the items. The items purchased are removed from the inventory
		// need to implement a "cart" that when purchasing adds to the cart and deletes when removed
		ArrayList<GroceryItem> cart = new ArrayList<GroceryItem>();
		boolean done =false;
		String ItemBought="";
		do{

				boolean buying =false;
				while(!buying) { //validation of item entered
					System.out.println("Enter the correct name of item being added to cart: ")	;
			 
					ItemBought=input.nextLine();

					for (GroceryItem c : data) {
				
						if (c.getName().equals(ItemBought)) {
							buying=true;
							System.out.println("Found: " +ItemBought);
						}
					}
				}
				for(int i=0;i<data.size();i++) {	//adding item to cart
				    
					GroceryItem newITem = data.get(i); 

					 if(newITem.name.equals(ItemBought.toLowerCase())) { //adding found inventory item in the cart
						 cart.add(newITem);
						 System.out.println("Adding item to cart: "+newITem);
						 //System.out.println(cart);
						 System.out.print("Would you like to add another item to the cart?(y/n): ");
						 String answer =(input.nextLine());
						 if(answer.equalsIgnoreCase("n")|| answer.equalsIgnoreCase("no")) { //not adding another item to the cart
							i=data.size(); //quits out of for loop
							done=true;
						 }
						 else if(answer.equalsIgnoreCase("y")|| answer.equalsIgnoreCase("yes")){ //adding more to the cart
							i=0;
							System.out.println("Enter name of item being added to cart: ")	;
							ItemBought=input.nextLine();
							done=false;
						 }
						 else { //invalid option entered : exception handling
							 System.out.println("That was not a valid yes or no option");
							 i=data.size();
							 done=false;
						 }
					 }
				}
		}while(done==false);
		
		//System.out.println(data.size()); verification tool

		System.out.println("Items in Cart: "); //prints out total items added to cart
		for(int j=0;j<cart.size();j++) {
			
			System.out.println(+(j+1)+". "+cart.get(j));
			data.remove(data.indexOf(cart.get(j)));

		}
//		System.out.println(data.size()); //verification tool
//		System.out.println(data);
		
						return data;
	}

	
	public static void searchItem(ArrayList<GroceryItem> data, Scanner input){
		//this method searched for items in the grocery store inventory using a sequential search. The program will let user know if we have the item and how many (instances) of the item we have
			boolean found = false;
		System.out.print("Search for Item: ");
		String finditem = (input.nextLine()); //user inputs item they intend to search grocery inventory for
		int count =0; 
		for(int i=0;i<data.size();i++) {	//sequential search method
				GroceryItem newITem = data.get(i); 
				if(newITem.name.equals(finditem.toLowerCase())) { //if item found, boolean = true, and count increases 
					
					found=true;
					count++;
				}

			}if(found==true){
				System.out.println(finditem+"'s found in inventory: "+count); //prints item found and how many times its listed in inventory
			}else {
				System.out.println("This item is not currently listed in our inventory.");
			}

		}
	
	
	public static void saveFile(String filename, ArrayList<GroceryItem> data) {
		//this method writes the newly modified inventory back to the original csv file
			try {
				FileWriter writer = new FileWriter(filename);
				
				for (GroceryItem a:data) {
					writer.write(a.toCSV() + "\n");
					writer.flush();
				}
				writer.close();
			} catch (Exception e) { //exception handling  when saving to the file
				System.out.println("Something went wrong when saving to file.");
			}
	}
	

	public static void main(String[] args) {
		//this is the main method of the program
		Scanner input = new Scanner(System.in);
		ArrayList<GroceryItem> data = readfile("src/project/stock.csv"); //reads in file and saves to data arraylist
		data = menu(input,data); //allows user to access data through a menu and make changes if desired or search for item
		saveFile("src/project/stock.csv", data); //saves newly modified inventory list to original csv file
		input.close(); //close scanner
		System.out.println("bye!"); //shows user the program has been terminated
		
	}

}
