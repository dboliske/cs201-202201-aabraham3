package project;
//Akkina Abraham
//4/28/22
//The Age restricted class is a child class to the parent class GroceryItem.
public class AgeRestrictedItem extends GroceryItem {
	int age;
	
	AgeRestrictedItem(){ //default constructors
		super(); //calls default constructor from parent class for name and price
		age = 0;
	}
	AgeRestrictedItem(String name,double price, int age){ //non-default constructors
		super(name,price); //calls non defaults constructor from parent class:name and price
		this.age = 0;
		setAge(age);
	}
	public int getAge() { //getter or accessor
		return age;
	}
	public void setAge(int age) { //setter or mutator
		if(age==18 || age==21) { //only sets if age is 18 or 21
		this.age = age;
		}
	}
	public String toString() {
		return super.toString() + ", Purchase Age Requirement: "+ age + " years old.";
	}
	public boolean validAge(int age) { //validates that age needs to be either 18 or 21 to purchase
		return(age==18 || age==21);
	}
	
	public boolean equals(Object obj) { //equality check between objects and qualifications for an age restricted item
		if (!super.equals(obj)) {
			return false;
		} else if (!(obj instanceof AgeRestrictedItem)) {
			return false;
		}
		
		AgeRestrictedItem b = (AgeRestrictedItem)obj;
		if (this.age != b.getAge()) {
			return false;
		}
		
		return true;
	}
	
	public String toCSV() { //returns in this format to csv when written
		return super.csvData() + "," + age;
	}
		
	}

