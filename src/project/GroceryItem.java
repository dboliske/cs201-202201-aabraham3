package project;
//Akkina Abraham
//4/28/22
//This is the parent class. Defines what a grocery item is at minimum(object of name and price)
public class GroceryItem {

	String name;
	double price;
	
	GroceryItem(){ //default constructors
		name = " ";
		price = 1.0;	
	}
	GroceryItem(String name,double price){ //non-default constructors
		this.name = " ";
		setName(name);
		this.price=1.0;
		setPrice(price);
	}
	public String getName() { //getter or accessor for name
		return name;
	}
	public double getPrice() { //getter or accessor for price
		return price;
	}
	public void setName(String name) { //setter or mutator for name
		this.name = name;
	}
	public void setPrice(double price) {//setter or mutator for price
		if(price>0) { //only sets its price is a positive numbers
		this.price = price;
		}
	}
	
	public String toString() { //returns string of name and price
		
		return name +","+price;
	}
	public boolean validPrice(double price) {
		return(price>0); //returns true if price is greater than zero, which indicates that it is valid pricing
	}
	
	public boolean equals(Object obj) { //equality check if object matches qualifications for a Grocery item
		if (!(obj instanceof GroceryItem)) {
			return false;
		}
		
		GroceryItem a = (GroceryItem)obj;
		if (!this.name.equals(a.getName())) {
			return false;
		} else if (this.price != a.getPrice()) {
			return false;
		}
		
		return true;
	}
	protected String csvData() { 
		return name + "," + price;
	}
	
	public String toCSV() {//returns in this format to csv when written
		return csvData();
	}
}
