package project;
//Akkina Abraham
//4/28/22
// This is the PerishableItem class that extens the parent class GroceryItem. It defines a perishable item(object with expiration)
public class PerishableItem extends GroceryItem {

	String expiration;
	
	PerishableItem(){ //default constructors
		super(); //retrieves default constructs from parent class: name and price
		expiration = " ";
	}
	PerishableItem(String name,double price,String expiration){ //non-default constructors
		super(name,price); //retrieves non-default constructors from parent class: name and price
		this.expiration = " ";
		setExpiration(expiration);
		}
	public String getExpiration() { //getter or accessor
		return expiration;
	}
	public void setExpiration(String expiration) { //setter or mutator
		this.expiration=expiration;
	}
	public String toString() { //to string retrives to string from parent class(name and prices) and overwrites with expiration added
	
		return super.toString()+"," +expiration;
	}

	public boolean equals(Object obj) { //equality check between object and qualifications of a perishable item
		if (!super.equals(obj)) {
			return false;
		} else if (!(obj instanceof PerishableItem)) {
			return false;
		}
		
		PerishableItem b = (PerishableItem)obj;
		if (this.expiration != b.getExpiration()) {
			return false;
		}
		
		return true;
	}
	public String toCSV() { //returns in this format to csv when written
		return super.csvData() + "," + expiration;
	}
}