package project;
//Akkina Abraham
//4/28/2022
//Modify Class , modifies item price,expiration,age....
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;



public class ModifyItems {
	
	
	public static ArrayList<GroceryItem> modifyMenu(ArrayList<GroceryItem> data, Scanner input) {
		boolean done =false;
		ArrayList matchingIndices = new ArrayList<Integer>();	
		matchingIndices = pickItem(data,input);
		
		System.out.println("\nModify Menu!");
		String [] options = {"Modify Price", "Modify Expiration Date", "Modify Age Restriction", "Exit"};

		do{
			for(int i=0;i<options.length;i++) {	//prints numbered menu in do loop until the boolean is true(exit is selected)
				System.out.println((i+1)+". "+options[i]);
			}
			System.out.println("Enter # of Choice: ");
			String choice = input.nextLine();
			switch(choice) {	
			case "1":
				//nodifies any objects price
				System.out.println("Modifying price");
				data=modifyPrice(matchingIndices, data,input);
				done=true;
				break;
			case "2":
				//modifies only perishable items expiration 
				System.out.println("Modifying expiration");
				data=modifyExpiration(matchingIndices, data,input);
				done=true;
				break;
			case "3":
				//modifies only age restricted items age
				System.out.println("Modifying Age Restriction");
				data=modifyAge(matchingIndices, data,input);
				done=true;
				break;
			case "4": //exit menu
				System.out.println("Returning to Menu...");
				done=true;
				break;

			default: //an invalid option was entered
				System.out.println("That was not a valid choice, try again!");
				return data;
		//		break;
			}
		} while (!done);
		
	return data;
	}// end of modifyMenu
	
	
	public static ArrayList<Integer> pickItem(ArrayList<GroceryItem> data, Scanner input){
		System.out.println("Modifying selected item in inventory\n");
		
		// Define variables used within method

		boolean modifying = false;
		String ItemModified=""; // Name of item user enters
		ArrayList<Integer> matchingIndices = new ArrayList<>();
	    GroceryItem element = new GroceryItem();
		
		// Collect item name to modify using a loop
	    
		while(!modifying) { //validate item
		System.out.println("Enter exact name of item to modify");
		System.out.println("or 'q' to return to Menu [q]:");
 
		ItemModified = (input.nextLine());

		if (ItemModified.contentEquals("q")) {
			return matchingIndices;
			} else {
			System.out.println("Looking up: " +ItemModified);
			System.out.println("and found ");
		}

		//Check if item is in the list - name entered correctly
		//
		// If not, end while loop, and return to main menu
		// If found, create an object of that element and
		// add it's index from arraylist into an array of integer
		// to be used later for modifying individually
		
		for (int i = 0; i < data.size(); i++) {
			element = data.get(i);

		    if (ItemModified.equals(element.getName())) {
		        matchingIndices.add(i);
		        System.out.println("Index: " +i +": " +element);
				modifying = true;
		    }
		}    
		    System.out.print("\n");
		    
		if (!modifying) {
			System.out.println("none in inventory");
		}
		// Print matches
		//matchingIndices.forEach(System.out::println);	
	}	
			
	return matchingIndices;		

	} // end of pickItem
	
	public static ArrayList<GroceryItem> modifyPrice(ArrayList<Integer> matchingIndices, ArrayList<GroceryItem> data, Scanner input){
		
		// Find the name of item to be modified
		GroceryItem element = new GroceryItem();
		String ItemModified="";
		boolean reply=true;
		double newPrice = 0.0;

		element = data.get(matchingIndices.get(0));
		ItemModified=(element.getName());
		System.out.println("Modifying price of "+ItemModified+ " in inventory\n");		
		
		try {
		while (reply) {
			System.out.print("Enter the new price, or 'q' to return to main menu [q]: ");
			newPrice = Double.parseDouble(input.nextLine());
			System.out.println("You entered: " +newPrice);
			if (newPrice>=0) {
				reply=false;
			}	else {
			reply=true;
			}
		}
		} catch (Exception e) {
		System.out.println("Something wrong. Returning to main menu");
		return data;
	}
		for (int i = 0; i < matchingIndices.size(); i++) {
		element = data.get(matchingIndices.get(i));
		element.setPrice(newPrice);
		System.out.println("Price set " +data.get(matchingIndices.get(i)));
	}
	return data;	
	} //end of modifyPrice
	
	public static ArrayList<GroceryItem> modifyExpiration(ArrayList<Integer> matchingIndices, ArrayList<GroceryItem> data, Scanner input){
		
		// Find the name of item to be modified
		GroceryItem element = new GroceryItem();
		String ItemModified="";
		String newDate="01/01/1900";  
		boolean reply=true;

		element = data.get(matchingIndices.get(0));
		ItemModified=(element.getName());
		System.out.println("Modifying Expiration of \""+ItemModified+ "\" in inventory\n");	
		
		System.out.print("Enter the new perish date: \n");
		newDate = (input.nextLine());
		System.out.println("New Expiration Date: " +newDate);
		
		try {
		for(int i=0;i<matchingIndices.size();i++) { 
////	Will modify the elements here
				element = data.get(matchingIndices.get(i));

////Down casting GroceryItem to PerishableItem
////Parent p = new Child();
////Child Instance of type Parent = Parent Instance
////Cast Parent as a new child
					GroceryItem perished = new PerishableItem();
					perished=element;
					PerishableItem perished1 = (PerishableItem)perished;
					perished1.setExpiration(newDate);
					System.out.println("Set as: " +data.get(matchingIndices.get(i)));
			
		}
		} catch (Exception e) {
		System.out.println("Not a perishable item. Returning to main menu");
		return data;
	}
		return data;
	} // end of modifyPerishable
	
	public static ArrayList<GroceryItem> modifyAge(ArrayList<Integer> matchingIndices, ArrayList<GroceryItem> data, Scanner input){
		
		// Find the name of item to be modified
		GroceryItem element = new GroceryItem();
		String ItemModified="";
		Integer newAge=21;  

		element = data.get(matchingIndices.get(0));
		ItemModified=(element.getName());
		System.out.println("Modifying Age Restriction of \""+ItemModified+ "\" in inventory");	
		System.out.print("Age Restrictions are either 18 or 21\n");
		try {
		System.out.print("Enter new Age Restriction [18 or 21]: \n");
		newAge = Integer.parseInt(input.nextLine());
		System.out.println("New Age Resriction: " +newAge);
		

		for(int i=0;i<matchingIndices.size();i++) { 
////	Will modify the elements here
				element = data.get(matchingIndices.get(i));

////Down casting GroceryItem to PerishableItem
////Parent p = new Child();
////Child Instance of type Parent = Parent Instance
////Cast Parent as a new child
					GroceryItem age = new AgeRestrictedItem();
					age=element;
					AgeRestrictedItem age1 = (AgeRestrictedItem)age;
					age1.setAge(newAge);
					System.out.println("Set as: " +data.get(matchingIndices.get(i)));
			
		}
		} catch (Exception e) {
		System.out.println("Not an Age Restricted item.");
		System.out.println("Or wrong value entered. Return to main menu\n");

		return data;
	}
		return data;
	} // end of modifyAge
	
} //end of class
